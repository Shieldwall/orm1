import psycopg2
import psycopg2.extras


class DatabaseError(Exception):
    pass


class NotFoundError(Exception):
    pass


class ModifiedError(Exception):
    pass


class Entity(object):
    db = None

    __delete_query = 'DELETE FROM "{table}" WHERE {table}_id=%s'
    __insert_query = 'INSERT INTO "{table}" ({columns}) VALUES ({placeholders}) RETURNING "{table}_id"'
    __list_query = 'SELECT * FROM "{table}"'
    __select_query = 'SELECT * FROM "{table}" WHERE {table}_id=%s'
    __update_query = 'UPDATE "{table}" SET {columns} WHERE {table}_id=%s'

    __parent_query = 'SELECT * FROM "{table}" WHERE {parent}_id=%s'
    __sibling_query = 'SELECT * FROM "{sibling}" NATURAL JOIN "{join_table}" WHERE {table}_id=%s'
    __update_children = 'UPDATE "{table}" SET {parent}_id=%s WHERE {table}_id IN ({children})'

    def __init__(self, id=None):
        if self.__class__.db is None:
            raise DatabaseError()

        self.__cursor = self.__class__.db.cursor(
            cursor_factory=psycopg2.extras.RealDictCursor
        )
        self.__fields = {}
        self.__id = id
        self.__loaded = False
        self.__modified = False
        self.__table = self.__class__.__name__.lower()

    def __getattr__(self, name):
        if self.__modified:
            raise ModifiedError()

        self.__load()

        if name in self.__class__._columns:
            return self._get_column(name)
        else:
            raise AttributeError()

    def __setattr__(self, name, value):
        if name in self.__class__._columns:
            self._set_column(name, value)
        else:
            super(Entity, self).__setattr__(name, value)

    def __execute_query(self, query, args=None):
        result = None
        self.__cursor.execute(query, args)

        try:
            result = self.__cursor.fetchall()
        except psycopg2.ProgrammingError:
            pass
        except:
            self.__class__.db.rollback()

        self.__class__.db.commit()

        return result

    def __insert(self):
        placeholders = []
        values = []
        fields = []

        for field, value in self.__fields.iteritems():
            placeholders.append('%s')
            values.append(value)
            fields.append(field)

        result = self.__execute_query(self.__class__.__insert_query.format(
            table=self.__table,
            columns=', '.join(fields),
            placeholders=', '.join(placeholders)
        ), values)[0]
        self.id = result[self._convert_name('id')]

    def __load(self):
        if self.__loaded or self.id is None:
            return

        row = self.__execute_query(self.__class__.__select_query.format(
            table=self.__table), [self.id])[0]

        for key, value in row.iteritems():
            self.__fields[key] = value

        self.__loaded = True
        self.__modified = False

    def __update(self):
        placeholders = []
        values = []
        for field, value in self.__fields.iteritems():
            placeholders.append('"{field}" = %s'.format(field=field))
            values.append(value)

        values.append(self.id)
        self.__execute_query(self.__class__.__update_query.format(
            table=self.__table,
            columns=', '.join(placeholders)
        ), values)

    def _get_children(self, name):
        pass

    def _get_column(self, name):
        return self.__fields[self._convert_name(name)]

    def _get_parent(self, name):
        pass

    def _get_siblings(self, name):
        pass

    def _set_column(self, name, value):
        self.__fields[self._convert_name(name)] = value
        self.__modified = True

    def _set_parent(self, name, value):
        pass

    def _convert_name(self, name):
        return '{table}_{name}'.format(table=self.__table, name=name)

    @classmethod
    def all(cls):
        instance_list = []
        temp = cls()
        query = cls.__list_query.format(
            table=temp.__class__.__name__.lower()
        )
        row_list = temp.__execute_query(query)
        for row in row_list:
            instance = cls(row[temp._convert_name('id')])
            for key, value in row.iteritems():
                instance.__fields[key] = value

            instance.__modified = False
            instance.__loaded = True
            instance_list.append(instance)

        return instance_list

    def delete(self):
        if self.__id is None:
            raise AttributeError

        self.__execute_query(self.__class__.__delete_query.format(
            table=self.__table
        ), [self.id])

        self.__cursor.close()

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, value):
        self.__id = value

    @property
    def created(self):
        return self.__fields[_convert_name('created')]

    @property
    def updated(self):
        return self.__fields[_convert_name('updated')]

    def save(self):
        if self.__modified:
            if self.id is None or self.__execute_query(
                    self.__class__.__select_query.format(
                        table=self.__table
                    ), [self.id]) is None:
                self.__insert()
            else:
                self.__update()

            self.__modified = False

    def printer(self):
        for i, j in self.__fields.iteritems():
            print '{} = {}'.format(i, j)
