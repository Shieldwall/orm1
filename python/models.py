from entity import *


class Section(Entity):
    _columns = ['title']
    _parents = []
    _children = {'categories': 'Category'}
    _siblings = {}


class Category(Entity):
    _columns = ['title']
    _parents = ['section']
    _children = {'posts': 'Post'}
    _siblings = {}


class Post(Entity):
    _columns = ['content', 'title']
    _parents = ['category']
    _children = {'comments': 'Comment'}
    _siblings = {'tags': 'Tag'}


class Comment(Entity):
    _columns = ['text']
    _parents = ['post', 'user']
    _children = {}
    _siblings = {}


class Tag(Entity):
    _columns = ['name']
    _parents = []
    _children = {}
    _siblings = {'posts': 'Post'}


class User(Entity):
    _columns = ['name', 'email', 'age']
    _parents = []
    _children = {'comments': 'Comment'}
    _siblings = {}


class Testtable(Entity):
    _columns = ['name', 'type', 'number']

if __name__ == "__main__":
    # section = Section()
    # section.title = "zalupa"
    # section.save()

    # for section in Section.all():
    #     print section.title
    # a = Testtable()
    # a.name = 'New name'
    # a.save()

    # a = Testtable()
    # a.name = 'Another name'
    # a.type = 'Very interesting content'
    # a.save()

    # a = Testtable()
    # a.name = 'Third name'
    # a.type = 'Very interesting content with some freakin\' "quotes"'
    # a.save()

    # a.name = 'Bugs are wonderful'
    # a.save()
    Testtable.db = psycopg2.connect(
        host='188.166.30.234',
        database='postgres',
        user='postgres',
        password='biatch'
    )
    a = Testtable(1)
    a.name
    a.printer()
    b = Testtable.all()
    b[5].printer()
